def label = "devops-${UUID.randomUUID().toString()}"

def notifySlack(STATUS, COLOR) {
	slackSend (color: COLOR, message: STATUS+" : " +  "${env.JOB_NAME} [${env.BUILD_NUMBER}] (${env.BUILD_URL})")
}

podTemplate(
	label: label, 
	containers: [
		//container image는 docker search 명령 이용
		containerTemplate(name: "docker", image: "docker:stable", ttyEnabled: true, command: "cat"),
		containerTemplate(name: "kubectl", image: "lachlanevenson/k8s-kubectl", command: "cat", ttyEnabled: true),
		containerTemplate(name: "scanner", image: "newtmitch/sonar-scanner", ttyEnabled: true, command: "cat")
		
	],
	//volume mount
	volumes: [
		hostPathVolume(hostPath: "/var/run/docker.sock", mountPath: "/var/run/docker.sock")
	]
) 
{
	node(label) {
		stage("Get Source") {
			git "https://gitlab.com/mbshin/ibm-pipeline-study.git"
		}

		//-- 환경변수 파일 읽어서 변수값 셋팅
		def props = readProperties  file:"deployment/pipeline.properties"
		def tag = props["version"]
		def dockerRegistry = props["dockerRegistry"]
		def credential_registry=props["credential_registry"]
		def image = props["image"]
		def deployment = props["deployment"]
		def service = props["service"]
		def ingress = props["ingress"]
		def selector_key = props["selector_key"]
		def selector_val = props["selector_val"]
		def namespace = props["namespace"]

		try {
			stage("Inspection Code") {
				container("scanner") {
					sh "sonar-scanner \
					-Dsonar.projectName=inspect-devops \
  			-Dsonar.projectKey=inspect-devops \
  -Dsonar.sources=. \
  -Dsonar.host.url=http://169.56.102.163:32455 \
  -Dsonar.login=623d731957f3df87c3522d9eb2396782a221b6c0"      
				}
			}
			
			stage("Build Microservice image") {
				container("docker") {
					docker.withRegistry("${dockerRegistry}", "${credential_registry}") {
						sh "docker build -f ./deployment/Dockerfile -t ${image}:${tag} ."
						sh "docker push ${image}:${tag}"
						sh "docker tag ${image}:${tag} ${image}:latest"
						sh "docker push ${image}:latest"
					}
				}
			}

			stage("Image Vulnerability Scanning") {
				container("docker"){
					aquaMicroscanner imageName: "${image}:latest", notCompliesCmd: "", onDisallowed: "ignore", outputFormat: "html"
				}
			}
			
			stage( "Clean Up Existing Deployments" ) {
				container("kubectl") {
					sh "kubectl delete deployments -n ${namespace} --selector=${selector_key}=${selector_val}"
				}
			}

			stage( "Deploy to Cluster" ) {
				container("kubectl") {
					sh "kubectl apply -n ${namespace} -f ${deployment}"
					sh "sleep 5"
					notifySlack("${currentBuild.currentResult}", "#00FF00")
				}
			}
		} catch(e) {
			currentBuild.result = "FAILED"
			notifySlack("${currentBuild.currentResult}", "#FF0000")
		}
	}
}